// Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.

console.log(fetch('https://jsonplaceholder.typicode.com/todos'))

// 4. Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.

let array = []

fetch('https://jsonplaceholder.typicode.com/todos')
    .then(response => response.json())
    .then(json => {
        json.map((item) => {
        console.log( array += item.title) 
  })
});


fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(
    The item "${json.title}" has a status of "${json.completed}"
    ));

// 5. Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(json));


// 6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.


 fetch('https://jsonplaceholder.typicode.com/todos', 
		{ method : 'POST',
		header: {
			'Content-type' : 'application/json'
		},
		body: JSON.stringify({
			completed: 'false',
			id : 201,
			title: 'Created a To Do list Item',
			userId: 1
		}),
})
	.then((response) => response.json())
	.then((json) => console.log(json));


 fetch('https://jsonplaceholder.typicode.com/todos/1', 
		{ method : 'PUT',
		headers: {
			'Content-type' : 'application/json'
		},
		body: JSON.stringify({
			dateCompleted: 'Pending',
			description: 'To update the my to do list with a different data structures',
			id: 1,
			status: 'Pending',
			title: 'Updated To Do List Item',
			userId: 1
		}),
})
	.then((response) => response.json())
	.then((json) => console.log(json));



 fetch('https://jsonplaceholder.typicode.com/todos/1', 
		{ method : 'PUT',
		headers : {
			'Content-type' : 'application/json'
		},
		body: JSON.stringify({
			title: '',
			description:'',
			status: '',
			dateCompleted: '',
			userId: '',
		}),
})
	.then((response) => response.json())
	.then((json) => console.log(json));


fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {'Content-type': 'application/json'
},
	body: JSON.stringify({
		title: 'delectus aut autem',
		status: 'Complete',
		dateCompleted: '07/09/21',
		userId: 1,
	}),
})
.then((response) => response.json())
.then((json) => console.log(json))


fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'DELETE'
});

